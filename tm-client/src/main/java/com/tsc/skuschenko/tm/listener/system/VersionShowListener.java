package com.tsc.skuschenko.tm.listener.system;

import com.jcabi.manifests.Manifests;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class VersionShowListener extends AbstractListener {

    private static final String ARGUMENT = "-v";

    private static final String DESCRIPTION = "version";

    private static final String NAME = "version";

    @Autowired
    private IPropertyService service;

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@versionShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showOperationInfo(NAME);
        System.out.println("VERSION: " + service.getApplicationVersion());
        System.out.println("BUILD: " + Manifests.read("build"));
    }

    @Override
    public String name() {
        return NAME;
    }

}